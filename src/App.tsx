import './App.css'
import {
  createBrowserRouter,
  RouterProvider,
} from 'react-router-dom';

import { isLoggedin, protectedRoute } from './pages/utils/protectedRoute'

import Home from './pages/Home';
import Login from './pages/Login/index';
import Register from './pages/Register/index';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    loader: protectedRoute
  },
  {
    path: "/Login",
    element: <Login />,
    loader: isLoggedin
  },
  {
    path: "/Register",
    element: <Register />,
    loader: protectedRoute
  },
  {
    path: "*",
    element: <h1>404 not found</h1>,
  },
  
])

function App() {


  return (
    <>
      <RouterProvider router={router} />
    </>
  )
}

export default App