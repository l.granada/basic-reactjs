import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {accounts} from "./../utils/authenAccounts"
import { IProp } from './type'


function Login(props: IProp) {
    const navigate = useNavigate();
    const [showPass, setShowPass] = useState<boolean>(false);
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    

    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setUsername(value);
      };
    
    const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setPassword(value);
      };

      const HandlerLogin = () => {
        const token = {
            username,
            password,
        };
        const authenticated = accounts.map((item) => JSON.stringify(item)).includes(JSON.stringify(token));
        if (authenticated) {
            localStorage.setItem("Token", JSON.stringify(token));
            navigate("/");
        } else {
            alert("Invalid Account");
        }
      };
    return (
    <div>
      <h1>Login</h1>
        <input value={username} type='text' placeholder="Email here..." onChange={emailHandler}></input><br />
        <input value={password} type={showPass ? "text" : "password"} placeholder="Password here..." onChange={passwordHandler}></input><br />
        <input type="checkbox" checked={showPass} onChange={() => setShowPass(prev => !prev)}/><span>See Passwords</span><br/><br />
            <button onClick={HandlerLogin} className="input-message">Log In</button>
    </div>
    )
}

export default Login;