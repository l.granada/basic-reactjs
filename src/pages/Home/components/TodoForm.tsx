import { ITodoFormProp } from "../type";
import './../style.css'


function TodoForm(props: ITodoFormProp) {
    const { inputHandler, messageHandler, btnAddHandler, temp_input, temp_message } = props;
  return (
    <div>
        <div className="form">
        
            <textarea
                className="input-message"
                name="title"
                
                onChange={inputHandler}
                value={temp_input}
                placeholder="Input here..."
            />
          <span className="input-border"></span>
            <textarea
                className="input-message"
                name="message"
                onChange={messageHandler}
                value={temp_message}
                placeholder="Message here..."
            />
            <span className="input-border"></span>
      <button className="btn" onClick={btnAddHandler}>
        Add
      </button>
      </div>
    </div>
  );
}

export default TodoForm;