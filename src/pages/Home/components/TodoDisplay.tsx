import "../style.css";
import { ITodoDisplayProp } from "../type";


function TodoDisplay(props: ITodoDisplayProp) {
  const { inputlist, showTable,  editItem, actionDelete, actionEdit, handleCancelButton, handleEditInput, handleUpdateButton } = props;
  
  return (
    <>
      {!showTable ? (
        <></>
      ) : (
        <table style={{ borderRadius: '10px'}} className="table-wrapper">
          <tbody>
            <tr>
              <th style={{ backgroundColor: "#ccc"}}>ID</th>
              <th style={{ backgroundColor: "#ccc" }}>Title</th>
              <th style={{ backgroundColor: "#ccc" }}>Message</th>
              <th style={{ backgroundColor: "#ccc" }}>Action</th>
            </tr>
            {inputlist.map((items, index) => {
              if (items.id === editItem?.id && items.id) {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <span>{items.id}</span>
                    </td>
                    <td>
                      <input name="editInput" type="text" value={editItem.input} onChange={handleEditInput}/>
                    </td>
                    <td>
                      <input name="editMessage" type="text" value={editItem.message} onChange={handleEditInput}/>
                    </td>
                    <td>
                      <button className="btn-btn-update" onClick={handleUpdateButton}>Update</button>
                      <button className="btn-btn-cancel" onClick={handleCancelButton}>Cancel</button>
                    </td>
                  </tr>
                );
              } else {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <span>{items.id}</span>
                    </td>
                    <td>
                      <span>{items.input}</span>
                    </td>
                    <td>{items.message}</td>
                    <td>
                      <button
                        className="btn-btn-primary"
                        onClick={() => actionEdit(items)}
                      >
                        Edit
                      </button>
                      <button
                        onClick={() => actionDelete(items.id)}
                        className="btn-btn-error"
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
      )}
    </>
  );
}

export default TodoDisplay;