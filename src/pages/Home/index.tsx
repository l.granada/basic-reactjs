import React, {useState} from "react";
import { ITodo, IState } from "./type";
import "./style.css";
import { useNavigate } from "react-router-dom";
import TodoDisplay  from "./components/TodoDisplay";
import TodoForm from './components/TodoForm'

function Home() {
  const navigate = useNavigate();
  const [state, setState] = useState<IState>({
       temp_input: "",
       temp_message: "",
       input_list: [],
       showTable: false,
       editItem: {
         id: '',
         input: '',
         message: ''
 }})

 const inputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
  const { value } = event.target;
  setState(state => ({ ...state, temp_input: value }));
};

  const messageHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setState(state => ({ ...state, temp_message: value }));
  };

  const btnAddHandler = () => {
    const { temp_input, temp_message, input_list } = state;
    if (temp_input === '' || temp_message === ''){
      alert('Please fill in the Blank');
      return
    }
    const newInputList = [
      ...input_list,
      {
        id: `${new Date().getTime() / 1000}`,
        input: temp_input,
        message: temp_message,
      },
    ];
    setState(state => ({ ...state, input_list: newInputList, temp_input: "", temp_message: "", }));
  
  };


  const handleCheckShowTable = (event: any) => {
    const { checked } = event.target;
    setState({ ...state, showTable: checked });
  };


  const handleActionDelete = (id: string) => {
    const { input_list } = state;
    const newInputList = input_list.filter((item) => item.id !== id);
    setState(state => ({ ...state, input_list: newInputList }));
  };


  const handleEditButton = (item: ITodo) => {
    setState(state => ({ ...state, editItem: item  }));
  };

  const handleEditInput = (event: any) => {
    const { name, value } = event.target;


    if (name === "editInput") {
      setState((state) => ({...state, editItem: { ...state.editItem, input: value }}));
    } else if (name === "editMessage") {
      setState((state) => ({ ...state, editItem: { ...state.editItem, message: value }}));
    }
  };

  const handleUpdateButton = () => {
    const { editItem, input_list  } = state;

    const newList = input_list.map(items => {
      if(items.id === editItem.id) {
        return editItem
      }
      else return items;
    })
    setState(state =>({ ...state, input_list: newList, editItem: {id: '', input: '', message: ''}}))
  }

  const handleCancelButton = () => {
    setState({...state, editItem: { id: '', input: '', message: '' } });
  }

  const handleLogoutButton = () => {
    navigate('./login');
  }
    return (
      <div className="center">
        <h1>Home Page</h1>
        <button onClick={handleLogoutButton} className="input-message">Log Out</button>
        <div className="basic-form-container">
          <TodoForm
            btnAddHandler={btnAddHandler}
            inputHandler={inputHandler}
            messageHandler={messageHandler}
            temp_input={state.temp_input}
            temp_message={state.temp_message}
          />
        </div>
        <input type="checkbox" onChange={handleCheckShowTable} />{" "}
        <span>Show Table</span>
        <TodoDisplay
          actionEdit={handleEditButton}
          editItem={state.editItem}
          actionDelete={handleActionDelete}
          showTable={state.showTable}
          inputlist={state.input_list}
          handleEditInput={handleEditInput}
          handleUpdateButton={handleUpdateButton}
          handleCancelButton={handleCancelButton}
        />
      </div>
    );
  

}

export default Home;